# Specyfikacja

Program *DMMBrymenTool* jest prostym narzędziem CLI do odczytu danych z multimetrów (DMM) firmy Brymen.

Obsługiwane typy multimetrów:
- Brymen BM869s (http://www.brymen.com/PD02BM860s_869s.html)

Obsługiwany interfejs komunikacyjny:
- Brymen BU-86X (protokół USB HID) (https://www.tme.eu/pl/en/details/kitbu-86x/meters-software/brymen)

Platforma: Windows 10+ 64-bit

Toolchain: Visual Studio Community 2022 v17.10.0

Język: C++ 2020 latest

Instalacja: nie jest wymagana. Nie są wymagane dedykowane sterowniki USB (używany jest protokół HID).

# Odczyt danych (v1.0)

Odczytane dane z multimetru są wyświetlane na konsoli w czasie rzeczywistym bez zapisywania do pliku.

Aktualna wersja programu pokazuje oryginalne wartości z miernika (także z nadmiarowymi cyframi oznaczającymi wybrany zakres pomiarowy).

Przykład odczytu:

```
15, 2022-01-08T08:02:15.327Z, primary: | 118.59 | mV AC, secondary: | 50.01| Hz, vfd: 0, auto: 0, hold: 0, delta: 0, crest: 0, rec: 0, ---/---/---
16, 2022-01-08T08:02:15.533Z, primary: | 117.99 | mV AC, secondary: | 50.01| Hz, vfd: 0, auto: 0, hold: 0, delta: 0, crest: 0, rec: 0, ---/---/---
17, 2022-01-08T08:02:15.723Z, primary: | 114.20 | mV AC, secondary: | 49.98| Hz, vfd: 0, auto: 0, hold: 0, delta: 0, crest: 0, rec: 0, ---/---/---
```

Kolumny od lewej:
  1. Licznik odczytów.
  2. Stempel czasu UTC.
  3. Wartość z głównego wyświetlacza 7-segmentowego + jednostka.
  4. Wartość z małego wyświetlacza 7-segmentowego + jednostka.
  5. Status funkcji VFD.
  6. Status funkcji AUTO.
  7. Status funkcji HOLD.
  8. Status funkcji DELTA.
  9. Status funkcji CREST.
  10. Status funkcji REC.
  11. Stan MIN, MAX i AVG dla funkcji CREST i REC.

# Licencja

MIT

# Support

No merge requests. If you want to report a bug, create an issue.
