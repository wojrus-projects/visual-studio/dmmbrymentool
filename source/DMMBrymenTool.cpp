//
// DMM Brymen CLI tool for logging purposes.
// Version: v1.0
// 
// Supported instruments:
// Brymen BM869s.
// 
// Supported interfaces:
// Brymen BU-86X (USB HID protocol).
// 
// Author: W.R. <rwxrwx@interia.pl>
// License: MIT
//

#include <cstdint>
#include <iostream>
#include <format>
#include <thread>
#include <chrono>
#include <map>
#include <array>
#include <optional>

#include "hidapi/hidapi.h"


namespace Utilities
{
    std::string getTimestamp()
    {
        const auto now = std::chrono::system_clock::now();

        return std::format("{:%FT%TZ}", std::chrono::time_point_cast<std::chrono::milliseconds>(now));
    }


    void delay(unsigned int time_ms)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(time_ms));
    }
} // namespace Utilities


namespace DMM::Brymen::BU_86X
{
    static constexpr unsigned int USB_HID_DATA_CHUNK_SIZE = 8;
    static constexpr unsigned int USB_HID_DATA_CHUNK_NUMBER = 3;
} // namespace DMM::Brymen::BU_86X


namespace DMM::Brymen::BM860
{
    enum class MeasurementQuantity
    {
        Unknown,
        Voltage,
        Current,
        CurrentLoopRatio,
        Resistance,
        ResistanceWithContinuity,
        DiodeTest,
        Conductance,
        Temperature_T1,
        Temperature_T2,
        Temperature_T1MinusT2,
        Capacitance,
        Frequency,
        Power,
        Duty
    };

    enum class MeasurementUnit
    {
        Unknown,
        V,
        mV,
        uV,
        A,
        mA,
        uA,
        PercentOf4_20mA,
        Ohm,
        kOhm,
        MOhm,
        nS,
        C,
        F,
        mF,
        uF,
        nF,
        Hz,
        kHz,
        dBm,
        PercentOfDuty
    };

    enum class MeasurementACDCFunction
    {
        Unknown,
        DC,
        AC,
        AC_DC
    };

    static const std::array<const char*, 21> MeasurementUnitNames =
    {
        "Unknown",
        "V",
        "mV",
        "uV",
        "A",
        "mA",
        "uA",
        "%4~20mA",
        "Ohm",
        "kOhm",
        "MOhm",
        "nS",
        "C",
        "F",
        "mF",
        "uF",
        "nF",
        "Hz",
        "kHz",
        "dBm",
        "D%"
    };

    static const std::array<const char*, 4> MeasurementACDCFunctionNames =
    {
        "Unknown",
        "DC",
        "AC",
        "AC+DC"
    };


    /// <summary>
    /// Measurement result for one display (primary or secondary).
    /// </summary>
    struct MeasurementResult final
    {
        // Selected quantity.
        MeasurementQuantity quantity = MeasurementQuantity::Unknown;

        // Quantity unit.
        MeasurementUnit unit = MeasurementUnit::Unknown;

        // Selected AC/DC function.
        MeasurementACDCFunction acdcFunction = MeasurementACDCFunction::Unknown;

        // Original (displayed) measurement result.
        std::string valueString;

        // Measurement result converted to float32 (if possible).
        std::optional<float> valueNumerical;


        void clear()
        {
            quantity = MeasurementQuantity::Unknown;
            unit = MeasurementUnit::Unknown;
            acdcFunction = MeasurementACDCFunction::Unknown;
            valueString = {};
            valueNumerical.reset();
        }


        std::string toString()
        {
            std::string str = std::format("|{}|", valueString);

            if (unit != MeasurementUnit::Unknown)
            {
                if (((unit == MeasurementUnit::C) || (unit == MeasurementUnit::F))
                    &&
                    ((valueString.find("C") != std::string::npos) || (valueString.find("F") != std::string::npos)))
                {
                    // Unit symbol is already in measurement value.
                }
                else
                {
                    str += std::format(" {}", MeasurementUnitNames[size_t(unit)]);
                }
            }

            if (quantity == MeasurementQuantity::Temperature_T1)
            {
                str += " T1";
            }
            else if (quantity == MeasurementQuantity::Temperature_T2)
            {
                str += " T2";
            }
            else if (quantity == MeasurementQuantity::Temperature_T1MinusT2)
            {
                str += " T1-T2";
            }
            else if (quantity == MeasurementQuantity::ResistanceWithContinuity)
            {
                str += " continuity";
            }

            if (acdcFunction != MeasurementACDCFunction::Unknown)
            {
                str += std::format(" {}", MeasurementACDCFunctionNames[size_t(acdcFunction)]);
            }

            return str;
        }
    }; // struct MeasurementResult


    /// <summary>
    /// DMM one measurement complete result.
    /// </summary>
    struct DMMMeasurementData final
    {
        MeasurementResult primary;
        MeasurementResult secondary;

        uint16_t isVFD : 1 = false;
        uint16_t isAuto : 1 = false;
        uint16_t isHold : 1 = false;
        uint16_t isDelta : 1 = false;
        uint16_t isCrest : 1 = false;
        uint16_t isRec : 1 = false;
        uint16_t isMin : 1 = false;
        uint16_t isMax : 1 = false;
        uint16_t isAvg : 1 = false;

        uint16_t isOpenLine : 1 = false;
        uint16_t isInputError : 1 = false;


        void clear()
        {
            primary.clear();
            secondary.clear();

            isVFD = false;
            isAuto = false;
            isHold = false;
            isDelta = false;
            isCrest = false;
            isRec = false;
            isMin = false;
            isMax = false;
            isAvg = false;

            isOpenLine = false;
            isInputError = false;
        }


        std::string toString()
        {
            std::string str = std::format("primary: {}, secondary: {}",
                primary.toString(), secondary.toString());

            str += std::format(", vfd: {}, auto: {}, hold: {}, delta: {}, crest: {}, rec: {}",
                (int)isVFD, (int)isAuto, (int)isHold, (int)isDelta, (int)isCrest, (int)isRec);

            str += std::format(", {}/{}/{}",
                isMin ? "MIN" : "---",
                isMax ? "MAX" : "---",
                isAvg ? "AVG" : "---");

            return str;
        }
    }; // struct DMMMeasurementData


    /// <summary>
    /// DMM LCD segment map.
    /// 
    /// Source: http://www.brymen.com/images/DownloadList/ProtocolList/BM860-BM860s_List/BM860-BM860s-500000-count-dual-display-DMMs-protocol.pdf
    /// TABLE 1. LCD map (without 'Report ID' 3 bytes).
    /// </summary>
#pragma pack(1)
    struct DMMDisplaySegmentMap final
    {
        // Byte No. 2
        uint8_t dontCare1 : 8;

        // Byte No. 3
        uint8_t AUTO : 1;
        uint8_t R : 1;
        uint8_t C : 1;
        uint8_t H : 1;
        uint8_t DC : 1;
        uint8_t MAX : 1;
        uint8_t MIN : 1;
        uint8_t AVG : 1;

        // Byte No. 4
        uint8_t primaryAC : 1;
        uint8_t T1 : 1;
        uint8_t Tminus : 1;
        uint8_t primaryT2 : 1;
        uint8_t barScale : 1;
        uint8_t barScaleMinus : 1;
        uint8_t VFD : 1;
        uint8_t primaryMinus : 1;

        // Byte No. 5
        uint8_t delta : 1;
        uint8_t digit_1e : 1;
        uint8_t digit_1f : 1;
        uint8_t digit_1a : 1;
        uint8_t digit_1d : 1;
        uint8_t digit_1c : 1;
        uint8_t digit_1g : 1;
        uint8_t digit_1b : 1;

        // Byte No. 6
        uint8_t digit_1p : 1;
        uint8_t digit_2e : 1;
        uint8_t digit_2f : 1;
        uint8_t digit_2a : 1;
        uint8_t digit_2d : 1;
        uint8_t digit_2c : 1;
        uint8_t digit_2g : 1;
        uint8_t digit_2b : 1;

        // Byte No. 7
        uint8_t digit_2p : 1;
        uint8_t digit_3e : 1;
        uint8_t digit_3f : 1;
        uint8_t digit_3a : 1;
        uint8_t digit_3d : 1;
        uint8_t digit_3c : 1;
        uint8_t digit_3g : 1;
        uint8_t digit_3b : 1;

        // Byte No. 8
        uint8_t digit_3p : 1;
        uint8_t digit_4e : 1;
        uint8_t digit_4f : 1;
        uint8_t digit_4a : 1;
        uint8_t digit_4d : 1;
        uint8_t digit_4c : 1;
        uint8_t digit_4g : 1;
        uint8_t digit_4b : 1;

        // Byte No. 9
        uint8_t digit_4p : 1;
        uint8_t digit_5e : 1;
        uint8_t digit_5f : 1;
        uint8_t digit_5a : 1;
        uint8_t digit_5d : 1;
        uint8_t digit_5c : 1;
        uint8_t digit_5g : 1;
        uint8_t digit_5b : 1;

        // Byte No. 11
        uint8_t primaryV : 1;
        uint8_t digit_6e : 1;
        uint8_t digit_6f : 1;
        uint8_t digit_6a : 1;
        uint8_t digit_6d : 1;
        uint8_t digit_6c : 1;
        uint8_t digit_6g : 1;
        uint8_t digit_6b : 1;

        // Byte No. 12
        uint8_t secondaryMicro : 1;
        uint8_t secondaryMilli : 1;
        uint8_t secondaryA : 1;
        uint8_t percent_currentLoop : 1;
        uint8_t secondaryMinus : 1;
        uint8_t secondaryAC : 1;
        uint8_t secondaryT2 : 1;
        uint8_t batteryLow : 1;

        // Byte No. 13
        uint8_t continuityTest : 1;
        uint8_t digit_7e : 1;
        uint8_t digit_7f : 1;
        uint8_t digit_7a : 1;
        uint8_t digit_7d : 1;
        uint8_t digit_7c : 1;
        uint8_t digit_7g : 1;
        uint8_t digit_7b : 1;

        // Byte No. 14
        uint8_t digit_7p : 1;
        uint8_t digit_8e : 1;
        uint8_t digit_8f : 1;
        uint8_t digit_8a : 1;
        uint8_t digit_8d : 1;
        uint8_t digit_8c : 1;
        uint8_t digit_8g : 1;
        uint8_t digit_8b : 1;

        // Byte No. 15
        uint8_t digit_8p : 1;
        uint8_t digit_9e : 1;
        uint8_t digit_9f : 1;
        uint8_t digit_9a : 1;
        uint8_t digit_9d : 1;
        uint8_t digit_9c : 1;
        uint8_t digit_9g : 1;
        uint8_t digit_9b : 1;

        // Byte No. 16
        uint8_t digit_9p : 1;
        uint8_t digit_10e : 1;
        uint8_t digit_10f : 1;
        uint8_t digit_10a : 1;
        uint8_t digit_10d : 1;
        uint8_t digit_10c : 1;
        uint8_t digit_10g : 1;
        uint8_t digit_10b : 1;

        // Byte No. 17
        uint8_t secondaryMega : 1;
        uint8_t secondaryKilo : 1;
        uint8_t secondaryHz : 1;
        uint8_t secondaryV : 1;
        uint8_t S : 1;
        uint8_t F : 1;
        uint8_t n : 1;
        uint8_t primaryA : 1;

        // Byte No. 18
        uint8_t primaryHz : 1;
        uint8_t dB : 1;
        uint8_t primaryMilli : 1;
        uint8_t primaryMicro : 1;
        uint8_t omega : 1;
        uint8_t primaryMega : 1;
        uint8_t primaryKilo : 1;
        uint8_t D_percent : 1;

        // Byte No. 20
        uint8_t dontCare2 : 8;

        // Byte No. 21
        uint8_t dontCare3 : 8;

        // Byte No. 22
        uint8_t dontCare4 : 8;

        // Byte No. 23
        uint8_t modelID : 8;

        // Byte No. 24
        uint8_t dontCare5 : 8;

        // Byte No. 25
        uint8_t dontCare6 : 8;

        // Byte No. 26
        uint8_t dontCare7 : 8;

        // Byte No. 27
        uint8_t dontCare8 : 8;
    }; // struct DMMDisplaySegmentMap


    /// <summary>
    /// Raw data read from DMM.
    /// </summary>
#pragma pack(1)
    union DMMDisplayData final
    {
        uint8_t usbHidData[BU_86X::USB_HID_DATA_CHUNK_SIZE * BU_86X::USB_HID_DATA_CHUNK_NUMBER];
        DMMDisplaySegmentMap segmentMap;


        DMMDisplayData() : usbHidData{}
        {
        }


        void clear()
        {
            memset(usbHidData, 0, sizeof(usbHidData));
        }


        void appendUsbHidData(const uint8_t* pData, unsigned int usbHidPacketIndex)
        {
            if (usbHidPacketIndex >= BU_86X::USB_HID_DATA_CHUNK_NUMBER)
            {
                throw std::out_of_range("invalid packet index");
            }

            memcpy(&usbHidData[BU_86X::USB_HID_DATA_CHUNK_SIZE * usbHidPacketIndex], pData, BU_86X::USB_HID_DATA_CHUNK_SIZE);
        }


        bool parseStringPrimary(std::string& primaryString)
        {
            char primaryDigits[6]{};

            for (size_t i = 0; i < sizeof(primaryDigits); i++)
            {
                if (parseDigitSegments(usbHidData[3 + i], &primaryDigits[i]) != true)
                {
                    return false;
                }
            }

            if (segmentMap.primaryMinus == true)
            {
                primaryString += "-";
            }
            else
            {
                primaryString += " ";
            }

            const auto primaryDotPosition = getPrimaryDotPosition();

            for (size_t i = 0; i < sizeof(primaryDigits); i++)
            {
                if (primaryDotPosition)
                {
                    if (i == primaryDotPosition.value())
                    {
                        primaryString += '.';
                    }
                }

                primaryString += primaryDigits[i];
            }

            if (primaryString == " 1nErr ")
            {
                primaryString = " InErr ";
            }

            return true;
        }


        bool parseStringSecondary(std::string& secondaryString)
        {
            char secondaryDigits[4]{};

            for (size_t i = 0; i < sizeof(secondaryDigits); i++)
            {
                if (parseDigitSegments(usbHidData[10 + i], &secondaryDigits[i]) != true)
                {
                    return false;
                }
            }

            if (segmentMap.secondaryMinus == true)
            {
                secondaryString += "-";
            }
            else
            {
                secondaryString += " ";
            }

            const auto secondaryDotPosition = getSecondaryDotPosition();

            for (size_t i = 0; i < sizeof(secondaryDigits); i++)
            {
                if (secondaryDotPosition)
                {
                    if (i == secondaryDotPosition.value())
                    {
                        secondaryString += '.';
                    }
                }

                secondaryString += secondaryDigits[i];
            }

            return true;
        }


        bool convertToMeasurementData(DMMMeasurementData& measurementData)
        {
            measurementData.clear();

            //
            // Get value strings.
            //

            if (parseStringPrimary(measurementData.primary.valueString) != true)
            {
                return false;
            }

            if (parseStringSecondary(measurementData.secondary.valueString) != true)
            {
                return false;
            }

            //
            // Get flags.
            //

            measurementData.isVFD = segmentMap.VFD;
            measurementData.isAuto = segmentMap.AUTO;
            measurementData.isDelta = segmentMap.delta;
            measurementData.isHold = segmentMap.H;
            measurementData.isCrest = segmentMap.C;
            measurementData.isMin = segmentMap.MIN;
            measurementData.isMax = segmentMap.MAX;
            measurementData.isAvg = segmentMap.AVG;
            measurementData.isRec = segmentMap.R;

            measurementData.isOpenLine = (measurementData.primary.valueString.find("0L") != std::string::npos);
            measurementData.isInputError = (measurementData.primary.valueString.find("InErr") != std::string::npos);

            //
            // Get quantity, unit, AC/DC function.
            //

            const auto [primaryQuantity, primaryUnit, primaryAcdcFunction] = getQuantityPrimary();

            measurementData.primary.quantity = primaryQuantity;
            measurementData.primary.unit = primaryUnit;
            measurementData.primary.acdcFunction = primaryAcdcFunction;

            const auto [secondaryQuantity, secondaryUnit, secondaryAcdcFunction] = getQuantitySecondary();

            measurementData.secondary.quantity = secondaryQuantity;
            measurementData.secondary.unit = secondaryUnit;
            measurementData.secondary.acdcFunction = secondaryAcdcFunction;

            return true;
        }


        bool parseDigitSegments(uint8_t digitRawByte, char* pResultCharacter)
        {
            static const std::map<uint8_t, char> conversionMap =
            {
                { 0b1011'1110, '0' }, // Also 'O'.
                { 0b1010'0000, '1' }, // Also 'I'.
                { 0b1101'1010, '2' },
                { 0b1111'1000, '3' },
                { 0b1110'0100, '4' },
                { 0b0111'1100, '5' }, // Also 'S'.
                { 0b0111'1110, '6' },
                { 0b1010'1000, '7' },
                { 0b1111'1110, '8' },
                { 0b1111'1100, '9' },
                { 0b0001'1110, 'C' },
                { 0b0101'1110, 'E' },
                { 0b0100'1110, 'F' },
                { 0b0001'0110, 'L' },
                { 0b0100'0000, '-' },
                { 0b1111'0010, 'd' },
                { 0b0010'0000, 'i' },
                { 0b0110'0010, 'n' },
                { 0b0111'0010, 'o' },
                { 0b0100'0010, 'r' },
                { 0b0000'0000, ' ' }
            };

            digitRawByte &= 0b1111'1110;

            const auto item = conversionMap.find(digitRawByte);

            if (item != conversionMap.end())
            {
                *pResultCharacter = item->second;

                return true;
            }

            return false;
        }


        std::optional<unsigned int> getPrimaryDotPosition()
        {
            std::optional<unsigned int> dotPosition;

            if (segmentMap.digit_1p == true)
            {
                dotPosition = std::make_optional(1);
            }
            else if (segmentMap.digit_2p == true)
            {
                dotPosition = std::make_optional(2);
            }
            else if (segmentMap.digit_3p == true)
            {
                dotPosition = std::make_optional(3);
            }
            else if (segmentMap.digit_4p == true)
            {
                dotPosition = std::make_optional(4);
            }

            return dotPosition;
        }


        std::optional<unsigned int> getSecondaryDotPosition()
        {
            std::optional<unsigned int> dotPosition;

            if (segmentMap.digit_7p == true)
            {
                dotPosition = std::make_optional(1);
            }
            else if (segmentMap.digit_8p == true)
            {
                dotPosition = std::make_optional(2);
            }
            else if (segmentMap.digit_9p == true)
            {
                dotPosition = std::make_optional(3);
            }

            return dotPosition;
        }


        MeasurementUnit getTemperatureUnit()
        {
            MeasurementUnit unitValue = MeasurementUnit::Unknown;
            char unitChar = '\0';

            if (parseDigitSegments(usbHidData[8], &unitChar) != true)
            {
                throw std::domain_error("parse error");
            }

            if (unitChar == 'C')
            {
                unitValue = MeasurementUnit::C;
            }
            else if (unitChar == 'F')
            {
                unitValue = MeasurementUnit::F;
            }
            else
            {
                throw std::domain_error("invalid temperature unit");
            }

            return unitValue;
        }


        MeasurementACDCFunction getPrimaryACDCFunction()
        {
            MeasurementACDCFunction function = MeasurementACDCFunction::Unknown;

            if ((segmentMap.DC == true) and (segmentMap.primaryAC == false))
            {
                function = MeasurementACDCFunction::DC;
            }
            else if ((segmentMap.DC == false) and (segmentMap.primaryAC == true))
            {
                function = MeasurementACDCFunction::AC;
            }
            else if ((segmentMap.DC == true) and (segmentMap.primaryAC == true))
            {
                function = MeasurementACDCFunction::AC_DC;
            }
            else
            {
                // Select quantity other than V or I.
            }

            return function;
        }


        MeasurementACDCFunction getSecondaryACFunction()
        {
            MeasurementACDCFunction function = MeasurementACDCFunction::Unknown;

            if (segmentMap.secondaryAC == true)
            {
                function = MeasurementACDCFunction::AC;
            }

            return function;
        }


        std::tuple<MeasurementQuantity, MeasurementUnit, MeasurementACDCFunction> getQuantityPrimary()
        {
            MeasurementQuantity quantity = MeasurementQuantity::Unknown;
            MeasurementUnit unit = MeasurementUnit::Unknown;
            MeasurementACDCFunction acdcFunction = MeasurementACDCFunction::Unknown;

            if (segmentMap.primaryV == true)
            {
                quantity = MeasurementQuantity::Voltage;

                if (segmentMap.primaryMilli == true)
                {
                    unit = MeasurementUnit::mV;
                }
                else
                {
                    unit = MeasurementUnit::V;
                }

                acdcFunction = getPrimaryACDCFunction();
            }
            else if (segmentMap.primaryA == true)
            {
                quantity = MeasurementQuantity::Current;

                if (segmentMap.primaryMilli == true)
                {
                    unit = MeasurementUnit::mA;
                }
                else if (segmentMap.primaryMicro == true)
                {
                    unit = MeasurementUnit::uA;
                }
                else
                {
                    unit = MeasurementUnit::A;
                }

                acdcFunction = getPrimaryACDCFunction();
            }
            else if (segmentMap.S == true)
            {
                quantity = MeasurementQuantity::Conductance;

                if (segmentMap.n == true)
                {
                    unit = MeasurementUnit::nS;
                }
                else
                {
                    throw std::domain_error("invalid conductance unit");
                }
            }
            else if ((segmentMap.omega == true) && (segmentMap.continuityTest == false))
            {
                quantity = MeasurementQuantity::Resistance;

                if (segmentMap.primaryKilo == true)
                {
                    unit = MeasurementUnit::kOhm;
                }
                else if (segmentMap.primaryMega == true)
                {
                    unit = MeasurementUnit::MOhm;
                }
                else
                {
                    unit = MeasurementUnit::Ohm;
                }
            }
            else if ((segmentMap.omega == true) && (segmentMap.continuityTest == true))
            {
                quantity = MeasurementQuantity::ResistanceWithContinuity;

                if (segmentMap.primaryKilo == true)
                {
                    unit = MeasurementUnit::kOhm;
                }
                else if (segmentMap.primaryMega == true)
                {
                    unit = MeasurementUnit::MOhm;
                }
                else
                {
                    unit = MeasurementUnit::Ohm;
                }
            }
            else if (segmentMap.F == true)
            {
                quantity = MeasurementQuantity::Capacitance;

                if (segmentMap.n == true)
                {
                    unit = MeasurementUnit::nF;
                }
                else if (segmentMap.primaryMicro == true)
                {
                    unit = MeasurementUnit::uF;
                }
                else if (segmentMap.primaryMilli == true)
                {
                    unit = MeasurementUnit::mF;
                }
                else
                {
                    throw std::domain_error("invalid capacitance unit");
                }
            }
            else if ((segmentMap.T1 == true) && (segmentMap.primaryT2 == false) && (segmentMap.Tminus == false))
            {
                quantity = MeasurementQuantity::Temperature_T1;
                unit = getTemperatureUnit();
            }
            else if ((segmentMap.T1 == false) && (segmentMap.primaryT2 == true) && (segmentMap.Tminus == false))
            {
                quantity = MeasurementQuantity::Temperature_T2;
                unit = getTemperatureUnit();
            }
            else if ((segmentMap.T1 == true) && (segmentMap.primaryT2 == true) && (segmentMap.Tminus == true))
            {
                quantity = MeasurementQuantity::Temperature_T1MinusT2;
                unit = getTemperatureUnit();
            }
            else if (segmentMap.dB == true)
            {
                quantity = MeasurementQuantity::Power;
                unit = MeasurementUnit::dBm;
            }
            else if (segmentMap.primaryHz == true)
            {
                quantity = MeasurementQuantity::Frequency;

                if (segmentMap.primaryKilo == true)
                {
                    unit = MeasurementUnit::kHz;
                }
                else
                {
                    unit = MeasurementUnit::Hz;
                }
            }
            else if (segmentMap.D_percent == true)
            {
                quantity = MeasurementQuantity::Duty;
                unit = MeasurementUnit::PercentOfDuty;
            }
            else
            {
                std::string valueString;

                if (parseStringSecondary(valueString) != true)
                {
                    throw std::domain_error("parse error");
                }

                if (valueString.find("diod") != std::string::npos)
                {
                    quantity = MeasurementQuantity::DiodeTest;
                    unit = MeasurementUnit::V;
                }
                else
                {
                    // Empty display.
                }
            }

            return { quantity, unit, acdcFunction };
        }


        std::tuple<MeasurementQuantity, MeasurementUnit, MeasurementACDCFunction> getQuantitySecondary()
        {
            MeasurementQuantity quantity = MeasurementQuantity::Unknown;
            MeasurementUnit unit = MeasurementUnit::Unknown;
            MeasurementACDCFunction acdcFunction = MeasurementACDCFunction::Unknown;

            if (segmentMap.secondaryV == true)
            {
                quantity = MeasurementQuantity::Voltage;

                if (segmentMap.secondaryMilli == true)
                {
                    unit = MeasurementUnit::mV;
                }
                else
                {
                    unit = MeasurementUnit::V;
                }

                acdcFunction = getSecondaryACFunction();
            }
            else if (segmentMap.secondaryA == true)
            {
                quantity = MeasurementQuantity::Current;

                if (segmentMap.secondaryMilli == true)
                {
                    unit = MeasurementUnit::mA;
                }
                else if (segmentMap.secondaryMicro == true)
                {
                    unit = MeasurementUnit::uA;
                }
                else
                {
                    unit = MeasurementUnit::A;
                }

                acdcFunction = getSecondaryACFunction();
            }
            else if (segmentMap.secondaryT2 == true)
            {
                quantity = MeasurementQuantity::Temperature_T2;
                unit = getTemperatureUnit();
            }
            else if (segmentMap.secondaryHz == true)
            {
                quantity = MeasurementQuantity::Frequency;

                if (segmentMap.primaryKilo == true)
                {
                    unit = MeasurementUnit::kHz;
                }
                else
                {
                    unit = MeasurementUnit::Hz;
                }
            }
            else if (segmentMap.percent_currentLoop == true)
            {
                quantity = MeasurementQuantity::CurrentLoopRatio;
                unit = MeasurementUnit::PercentOf4_20mA;
            }
            else
            {
                std::string valueString;

                if (parseStringSecondary(valueString) != true)
                {
                    throw std::domain_error("parse error");
                }

                if (valueString.find("diod") != std::string::npos)
                {
                    quantity = MeasurementQuantity::DiodeTest;
                    unit = MeasurementUnit::Unknown;
                }
                else
                {
                    // Empty display.
                }
            }

            return { quantity, unit, acdcFunction };
        }
    }; // union DMMDisplayData


    static_assert(sizeof(DMMDisplaySegmentMap) == (BU_86X::USB_HID_DATA_CHUNK_SIZE * BU_86X::USB_HID_DATA_CHUNK_NUMBER));
    static_assert(sizeof(DMMDisplayData) == (BU_86X::USB_HID_DATA_CHUNK_SIZE * BU_86X::USB_HID_DATA_CHUNK_NUMBER));

} // namespace DMM::Brymen::BM860


namespace DMM::Brymen::BU_86X
{
    /// <summary>
    /// Brymen BU-86X interface.
    /// </summary>
    class BU_86X final
    {
    public:
        static constexpr uint16_t VID = 0x0820;
        static constexpr uint16_t PID = 0x0001;

    private:
        hid_device* pDevice = nullptr;

    public:
        BU_86X() = default;


        ~BU_86X()
        {
            hid_exit();
        }


        bool initialize()
        {
            if (hid_init() != 0)
            {
                return false;
            }

            hid_device_info* pDeviceInfo = hid_enumerate(VID, PID);
            if (pDeviceInfo == nullptr)
            {
                std::cout << "Error: USB interface not found\n";
                return false;
            }

            hid_free_enumeration(pDeviceInfo);

            return true;
        }


        bool open()
        {
            pDevice = hid_open(VID, PID, nullptr);
            if (pDevice == nullptr)
            {
                return false;
            }

            hid_set_nonblocking(pDevice, 1);

            return true;
        }


        bool close()
        {
            hid_close(pDevice);
            pDevice = nullptr;

            return true;
        }


        bool read(BM860::DMMDisplayData& dmmData)
        {
            constexpr bool PRINT_RAW_DATA = false;

            const uint8_t hidReportCommand[4] =
            {
                0x00,   // Report ID
                0x00,   // Command 1
                0x86,   // Command 2
                0x66    // Command 3
            };

            int hidPacketCounter = 0;
            int timeoutCounter = 0;
            bool isTimeout = false;
            bool isReadCompleted = false;
            bool isReadError = false;

            dmmData.clear();

            int resultWrite = hid_write(pDevice, hidReportCommand, sizeof(hidReportCommand));
            if (resultWrite != sizeof(hidReportCommand))
            {
                return false;
            }

            while ((isReadCompleted == false) && (isReadError == false) && (isTimeout == false))
            {
                constexpr int HID_READ_TIMEOUT_ms = 50;
                constexpr int TOTAL_READ_TIMEOUT_ms = 5'000;

                int resultRead;
                uint8_t readBuffer[DMM::Brymen::BU_86X::USB_HID_DATA_CHUNK_SIZE];

                resultRead = hid_read_timeout(pDevice, readBuffer, sizeof(readBuffer), HID_READ_TIMEOUT_ms);
                if (resultRead < 0)
                {
                    isReadError = true;
                }
                else if (resultRead == 0)
                {
                    timeoutCounter += HID_READ_TIMEOUT_ms;
                    if (timeoutCounter >= TOTAL_READ_TIMEOUT_ms)
                    {
                        isTimeout = true;
                    }
                }
                else
                {
                    if constexpr (PRINT_RAW_DATA)
                    {
                        std::cout << std::format("{}: hid_read()={}: ", hidPacketCounter, resultRead);

                        for (size_t i = 0; i < resultRead; i++)
                        {
                            std::cout << std::format("{:02X} ", readBuffer[i]);
                        }

                        std::cout << "\n";
                    }

                    dmmData.appendUsbHidData(readBuffer, hidPacketCounter);

                    if (++hidPacketCounter == DMM::Brymen::BU_86X::USB_HID_DATA_CHUNK_NUMBER)
                    {
                        isReadCompleted = true;
                    }
                }
            }

            return (isReadCompleted == true);
        }
    }; // class BU_86X
} // namespace DMM::Brymen::BU_86X


static void dmmMeasurementProcess(DMM::Brymen::BU_86X::BU_86X& usbInterface)
{
    bool isInterfaceOpened = false;

    while (true)
    {
        if (isInterfaceOpened == false)
        {
            Utilities::delay(500);

            if (usbInterface.open() == true)
            {
                isInterfaceOpened = true;
            }
            else
            {
                std::cout << "Error: USB interface open failure\n";
            }
        }
        else
        {
            DMM::Brymen::BM860::DMMDisplayData displayData;
            DMM::Brymen::BM860::DMMMeasurementData measurementData;

            if (usbInterface.read(displayData) == true)
            {
                if (displayData.convertToMeasurementData(measurementData) == true)
                {
                    static uint64_t measurementCounter = 0;

                    std::cout << std::format("{}, {}, {}\n", measurementCounter, Utilities::getTimestamp(), measurementData.toString());

                    measurementCounter++;
                }
                else
                {
                    std::cout << "Error: invalid DMM data format\n";
                }

                Utilities::delay(50);
            }
            else
            {
                std::cout << "Error: USB interface read failure\n";

                usbInterface.close();
                isInterfaceOpened = false;
            }
        }
    }
}


int main(int argc, char* argv[])
{
    DMM::Brymen::BU_86X::BU_86X usbInterface;

    usbInterface.initialize();
    
    dmmMeasurementProcess(usbInterface);

    return EXIT_SUCCESS;
}
